<?php

namespace App\Exception\Service;

use Exception;

class ServiceIsNotStartedException extends Exception
{
    protected $message = 'Action can not be performed as Service has not started yet';
}