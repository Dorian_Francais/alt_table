<?php

namespace App\Exception\Dish;

use Exception;

class MissingDishArgumentsException extends Exception
{
    protected $message = "They're missing arguments in your request.";
}
