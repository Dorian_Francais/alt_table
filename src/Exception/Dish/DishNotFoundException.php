<?php

namespace App\Exception\Dish;

use Exception;

class DishNotFoundException extends Exception
{
    protected $message = 'Dish not found';
}
