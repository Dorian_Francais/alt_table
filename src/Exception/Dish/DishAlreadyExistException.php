<?php

namespace App\Exception\Dish;

use Exception;

class DishAlreadyExistException extends Exception
{
    protected $message = 'A dish with the same name already exist.';
}
