<?php

namespace App\Exception\Dish;

use Exception;

class UnknownTypeException extends Exception
{
    protected $message = 'This type of dish is unknown.';
}
