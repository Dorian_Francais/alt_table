<?php

namespace App\Exception\Dish;

use Exception;

class DishesUnavailableException extends Exception
{
}
