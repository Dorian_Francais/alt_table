<?php

namespace App\Exception\Seating;

use Exception;

class SeatingNotFoundException extends Exception
{
    protected $message = 'Seating not found.';
}