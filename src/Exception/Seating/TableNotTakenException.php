<?php

namespace App\Exception\Seating;

use Exception;

class TableNotTakenException extends Exception
{
    protected $message = 'This table is not taken';
}