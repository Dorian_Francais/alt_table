<?php

namespace App\Exception\Seating;

class TableAlreadyTakenException extends \Exception
{
    const MESSAGE = 'This table is already full of customers';

    protected $message = self::MESSAGE;
}