<?php

namespace App\Exception\Seating;

class NumberOfClientsHigherThanAvailableSeatsException extends \Exception
{
    const MESSAGE = 'This table can not have this much clients.';

    protected $message = self::MESSAGE;
}