<?php

namespace App\Service;

use App\Entity\Service;
use App\Repository\SeatingRepository;

class ServiceSeatingService
{
    public function __construct(
        private SeatingRepository $seatingRepository
    )
    {
    }

    public function setSeatsToService(array $seats, Service $service): void
    {
        $this->seatingRepository->linkToService($seats, $service);
    }
}