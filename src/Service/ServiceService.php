<?php

namespace App\Service;

use App\Entity\Dto\ServiceRequest;
use App\Entity\Service;
use App\Exception\EntityCouldNotBeInsertedException;
use App\Exception\Service\ServiceIsNotStartedException;
use App\Repository\ServiceRepository;
use Carbon\Exceptions\InvalidFormatException;
use Exception;

class ServiceService
{
    public function __construct(
        private ServiceRepository     $serviceRepository,
        private SeatingService        $seatingService,
        private ServiceSeatingService $serviceSeatingService
    )
    {
    }

    public function launchService(): void
    {
        try {
            $this->seatingService->denyUnlessSeatingPlanIsActive();
            $service = $this->serviceRepository->launchService();

            $this->serviceSeatingService->setSeatsToService(
                $this->seatingService->getUnactiveSeats(),
                $service
            );
        } catch (InvalidFormatException) {
            throw new EntityCouldNotBeInsertedException("Service startDate is invalid");
        } catch (Exception $e) {
            throw new EntityCouldNotBeInsertedException($e->getMessage());
        }
    }

    public function getCurrentService(): ?Service
    {
        $currentService = $this->serviceRepository->getLastStartedService();

        if (!$currentService) {
            throw  new ServiceIsNotStartedException();
        }

        return $currentService;
    }

    public function stopCurrentService(Service $service): Service
    {
        return $this->serviceRepository->stopService($service);
    }
}