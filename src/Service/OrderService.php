<?php

namespace App\Service;

use App\Entity\Dto\Input\OrderDTO;
use App\Entity\Order;
use App\Entity\Seating;
use App\Entity\Service;
use App\Exception\Dish\DishesUnavailableException;
use App\Exception\Seating\TableNotTakenException;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;


class OrderService
{
    public function __construct(
        private OrderRepository        $orderRepository,
        private EntityManagerInterface $manager,
        private DishService            $dishService
    )
    {
    }

    public function createOrder(OrderDTO $orderRequestDTO, Service $service): Order
    {
        $seating = $this->manager->getRepository(Seating::class)->getTableByNumber($orderRequestDTO->seating_number, $service);

        if (!$seating->getIsTaken()) {
            throw new TableNotTakenException();
        }

        $errors = null;
        $dishes = [];

        foreach ($orderRequestDTO->dishs as $dish) {
            $dish = $this->dishService->getDishByName($dish);

            if ($dish->getQuantity() < 1) {
                $errors .= "Dish named {$dish->getName()} is unavailable. ";
                continue;
            }

            $dishes[] = $dish;

            $this->dishService->decrementDishQuantity($dish);
        }

        if (!empty($errors)) {
            throw new DishesUnavailableException($errors);
        }

        return $this->orderRepository->persistOrder($orderRequestDTO->comments, $seating, $dishes);
    }

}
