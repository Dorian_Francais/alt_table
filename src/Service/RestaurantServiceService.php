<?php

namespace App\Service;

use App\Entity\Seating;
use App\Exception\Service\ServiceIsNotStartedException;
use App\Exception\Service\ServiceStartedException;
use App\Repository\ServiceRepository;

class RestaurantServiceService
{
    public function __construct(
        private ServiceRepository $serviceRepository
    )
    {
    }

    public function denyUnlessServiceIsStopped(): void
    {
        if ($this->serviceRepository->findOneBy([], ['id' => 'DESC'])) {
            throw new ServiceStartedException();
        }
    }

    public function denyUnlessServiceIsStarted(Seating $table): void
    {
        if (!$table->getLinkedService()) {
            throw new ServiceIsNotStartedException();
        }
    }
}