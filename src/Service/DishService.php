<?php

namespace App\Service;

use App\Entity\Dish;
use App\Entity\Dto\Input\DishDTO;
use App\Exception\Dish\DishAlreadyExistException;
use App\Exception\Dish\DishNotFoundException;
use App\Exception\Dish\UnknownTypeException;
use App\Repository\DishRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;


class DishService
{
    public function __construct(
        private DishRepository         $dishRepository,
        private EntityManagerInterface $manager
    )
    {
    }

    public function getDishByName(string $name): ?Dish
    {
        $dish = $this->dishRepository->findOneByName($name);

        if (!$dish) {
            throw new DishNotFoundException();
        }

        return $dish;
    }

    public function createDish(DishDTO $dishRequestDTO): Dish
    {
        $dishesTypes = ["Apéritif", "Entrée", "Plat principal", "Dessert", "Boisson"];

        if (!in_array($dishRequestDTO->type, $dishesTypes, true)) {
            throw new UnknownTypeException();
        }

        if ($this->dishRepository->findOneBy(['name' => $dishRequestDTO->name])) {
            throw new DishAlreadyExistException();
        }

        return $this->dishRepository->persistDish($dishRequestDTO);
    }

    public function editDishQuantity(?int $quantity, ?string $name): ?Dish
    {
        try {
            $newQuantity = $this->dishRepository->persistEditDishQuantity($quantity, $name);

        } catch (Exception $e) {
            throw new DishNotFoundException();
        }
        return $newQuantity;
    }

    public function decrementDishQuantity(Dish $dish): Dish
    {
        return $this->dishRepository->decrementDishQuantity($dish);
    }

    public function deleteDish(string $name): void
    {
        $dish = $this->getDishByName($name);

        $this->manager->remove($dish);
        $this->manager->flush();
    }


    public function getAllDishes(): array
    {
        return $this->dishRepository->findAllDishes();
    }

    public function getMenu(): array
    {
        return $this->dishRepository->findMenu();
    }

}
