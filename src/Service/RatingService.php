<?php

namespace App\Service;

use App\Entity\Dish;
use App\Entity\Dto\Input\RatingDTO;
use App\Entity\Rating;
use App\Entity\Seating;
use App\Exception\Order\OrderNotFoundException;
use App\Exception\Rating\RatingCouldNotBeDoneException;
use App\Repository\OrderRepository;
use App\Repository\RatingRepository;


class RatingService
{
    public function __construct(
        private RatingRepository $ratingRepository,
        private OrderRepository  $orderRepository
    )
    {

    }

    public function createRating(RatingDTO $ratingRequestDTO, Dish $dish, Seating $seating): Rating
    {
        if ($existingRating = $this->ratingRepository->getExistingRatingByDishAndSeating($dish, $seating)) {
            $this->ratingRepository->removeRating($existingRating);
        }

        $currentOrder = $this->orderRepository->getCurrentOrderBySeating($seating);

        if (!$currentOrder) {
            throw new OrderNotFoundException();
        }

        $dishExists = false;
        foreach ($currentOrder->getDishes()->toArray() as $dishEntity) {
            if ($dishEntity->getName() === $ratingRequestDTO->dishName) {
                $dishExists = true;
                break;
            }
        }

        if (!$dishExists) {
            throw new RatingCouldNotBeDoneException("Dish name is not in the current order of the seating");
        }

        return $this->ratingRepository->persistAddRating($ratingRequestDTO, $dish, $seating);
    }

}
