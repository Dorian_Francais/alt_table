<?php

namespace App\Service;

use App\Entity\Seating;
use App\Entity\Service;
use App\Exception\EntityCouldNotBeInsertedException;
use App\Exception\Seating\CouldNotUpdateSeatingArrangementException;
use App\Exception\Seating\NumberOfClientsHigherThanAvailableSeatsException;
use App\Exception\Seating\SeatingNotFoundException;
use App\Exception\Seating\SeatingPlanNotActiveException;
use App\Exception\Seating\TableAlreadyTakenException;
use App\Exception\Seating\TableNotTakenException;
use App\Repository\SeatingRepository;
use Exception;

class SeatingService
{
    public function __construct(
        private SeatingRepository $seatingRepository
    )
    {
    }

    /**
     * @throws EntityCouldNotBeInsertedException
     */
    public function persistSeat(array $table): void
    {
        try {
            $this->seatingRepository->addSeating($table);
        } catch (Exception $e) {
            throw new EntityCouldNotBeInsertedException("Seating could not be inserted.");
        }
    }

    /**
     * @throws SeatingPlanNotActiveException
     */
    public function denyUnlessSeatingPlanIsActive(): bool
    {
        $seatingPlan = $this->seatingRepository->findAll();

        if ($seatingPlan === []) {
            throw new SeatingPlanNotActiveException();
        }

        return true;
    }

    public function isPreviousSeating(): void
    {
        if ($this->seatingRepository->checkForPreviousSeating()) {
            $this->seatingRepository->cleanPreviousSeating();
        }
    }

    public function getTableByNumber(int $tableNumber, Service $service): Seating
    {
        return $this->seatingRepository->getTableByNumber($tableNumber, $service);
    }

    /**
     * @throws CouldNotUpdateSeatingArrangementException
     */
    public function seatClientToTable(Seating $table, int $takenSeats): void
    {
        try {
            if ($table->getNbOfPlaces() < $takenSeats) {
                throw new NumberOfClientsHigherThanAvailableSeatsException();
            }
            if ($table->getIsTaken()) {
                throw new TableAlreadyTakenException();
            }
            $this->seatingRepository->seatClientToTable($table, $takenSeats);
        } catch (Exception $e) {
            throw new CouldNotUpdateSeatingArrangementException($e->getMessage());
        }
    }

    public function getUnactiveSeats(): array
    {
        return $this->seatingRepository->getUnactiveSeats();
    }

    public function refreshSeats(Seating $table): void
    {
        $this->seatingRepository->refreshSeats($table);
    }

    public function denyUnlessTableIsTaken(int $seatingNumber): void
    {
        $seating = $this->seatingRepository->findOneBy(['number' => $seatingNumber]);

        if (!$seating) {
            throw new SeatingNotFoundException();
        }

        if (!$seating->getIsTaken()) {
            throw new TableNotTakenException();
        }
    }
}