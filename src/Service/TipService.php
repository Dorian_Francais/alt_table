<?php

namespace App\Service;

use App\Entity\Seating;
use App\Repository\TipRepository;

class TipService
{
    public function __construct(
        private TipRepository $tipRepository
    )
    {
    }

    public function addTipToTable(Seating $table, float $amount): void
    {
        $this->tipRepository->addTipToTable($table, $amount);
    }
}