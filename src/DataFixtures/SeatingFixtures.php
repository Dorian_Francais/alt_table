<?php

namespace App\DataFixtures;

use App\Entity\Seating;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SeatingFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i=0;$i<5;$i++){
            $seating = new Seating();
            $seating->setNumber($i);
            $seating->setNbOfPlaces($i + 1);
            $manager->persist($seating);
        }
        $manager->flush();
    }
}
