<?php

namespace App\DataFixtures;

use App\Entity\Dish;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DishFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $dish = new Dish();

        $dish->setName("Boeuf1");
        $dish->setDescription("Test description");
        $dish->setType("Plat principal");
        $dish->setPrice(100.5);
        $dish->setQuantity(20);

        $manager->persist($dish);

        $dish2 = new Dish();

        $dish2->setName("Boeuf2");
        $dish2->setDescription("Test description");
        $dish2->setType("Plat principal");
        $dish2->setPrice(100.5);
        $dish2->setQuantity(20);

        $manager->persist($dish2);

        $manager->flush();
    }
}
