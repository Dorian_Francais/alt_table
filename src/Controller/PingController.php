<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/api/ping", name: "ping")]
class PingController extends AbstractController
{
    public function __invoke(): Response
    {
        return $this->json([
            'status' => 'OK',
        ]);
    }
}