<?php

namespace App\Controller;

use App\Entity\Dto\Input\DishDTO;
use App\Entity\Dto\Input\EditDishDTO;
use App\Service\DishService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DishController extends AbstractController
{
    public function __construct(
        private DishService $dishService
    )
    {
    }

    #[Route("/api/dish", name: "dish.add", methods: Request::METHOD_POST)]
    public function addDish(DishDTO $dishRequestDTO): JsonResponse
    {
        try {
            $returnedDish = $this->dishService->createDish($dishRequestDTO);

            return $this->json(["idDish" => $returnedDish->getId()], Response::HTTP_CREATED,);
        } catch (Exception $e) {
            return $this->json(["message" => $e->getMessage()], Response::HTTP_BAD_REQUEST,);
        }
    }

    #[Route("/api/dish/edit", name: "dish.edit", methods: Request::METHOD_PUT)]
    public function editDish(EditDishDTO $editDishDTO): JsonResponse
    {
        try {
            $this->dishService->editDishQuantity($editDishDTO->quantity, $editDishDTO->name);
            return $this->json(["response" => "Dish quantity updated"]);
        } catch (Exception $e) {
            return $this->json(["message" => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/api/dish/remove/{name}", name: "dish.remove", methods: Request::METHOD_DELETE)]
    public function removeDish(string $name): JsonResponse
    {
        try {
            $this->dishService->deleteDish($name);
        } catch (Exception $e) {
            return $this->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(["message" => "OK"]);

    }

    #[Route("/api/dishes", name: "dishes.get", methods: Request::METHOD_GET)]
    public function getAllDishes(): JsonResponse
    {
        try {
            $returnedDish = $this->dishService->getAllDishes();

            return $this->json(["Dishes" => $returnedDish]);
        } catch (Exception $e) {
            return $this->json(["message" => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/api/menu", name: "menu.get", methods: Request::METHOD_GET)]
    public function getMenu(): JsonResponse
    {
        try {
            $menu = $this->dishService->getMenu();

            return $this->json(["Menu" => $menu]);
        } catch (Exception $e) {
            return $this->json(["message" => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}