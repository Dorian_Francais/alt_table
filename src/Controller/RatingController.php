<?php

namespace App\Controller;

use App\Entity\Dto\Input\RatingDTO;
use App\Service\DishService;
use App\Service\RatingService;
use App\Service\SeatingService;
use App\Service\ServiceService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class RatingController extends AbstractController
{

    public function __construct(
        private RatingService  $ratingService,
        private DishService    $dishService,
        private SeatingService $seatingService,
        private ServiceService $serviceService
    )
    {
    }

    #[Route("/api/dish/rating", name: "rating.add", methods: Request::METHOD_POST)]
    public function addDishRating(RatingDTO $ratingRequestDTO): JsonResponse
    {
        try {
            $service = $this->serviceService->getCurrentService();
            $dish = $this->dishService->getDishByName($ratingRequestDTO->dishName);
            $seating = $this->seatingService->getTableByNumber($ratingRequestDTO->seatingNumber, $service);


            $rating = $this->ratingService->createRating($ratingRequestDTO, $dish, $seating);

            return $this->json(["dishRating" => $rating->getRate(), "dishName" => $dish->getName(), "seating" => $seating->getNumber()], Response::HTTP_CREATED,);
        } catch (Exception $e) {
            return $this->json(["message" => $e->getMessage()], Response::HTTP_BAD_REQUEST,);
        }
    }

}