<?php

namespace App\Controller;

use App\Entity\Dto\ServiceRequest;
use App\Service\ServiceService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ServiceController extends AbstractController
{
    public function __construct(
        private ServiceService $serviceService
    )
    {
    }

    #[Route("/api/service", methods: [Request::METHOD_POST])]
    public function launchService(): Response
    {
        try {
            $this->serviceService->launchService();
        } catch (Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return $this->json(["message" => "OK"]);
    }
}