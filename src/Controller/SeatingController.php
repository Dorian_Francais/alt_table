<?php

namespace App\Controller;

use App\Entity\Dto\LeaveSeatRequest;
use App\Entity\Dto\SeatClientRequest;
use App\Entity\Dto\SeatingPlanRequest;
use App\Entity\Validator\SeatingValidator;
use App\Service\RestaurantServiceService;
use App\Service\SeatingService;
use App\Service\ServiceService;
use App\Service\TipService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SeatingController extends AbstractController
{
    public function __construct(
        private SeatingService           $seatingService,
        private SeatingValidator         $seatingValidator,
        private RestaurantServiceService $restaurantServiceService,
        private ServiceService           $serviceService,
        private TipService               $tipService
    )
    {
    }

    #[Route("/api/seating", methods: [Request::METHOD_POST])]
    public function createSeating(SeatingPlanRequest $seatingPlanRequest): Response
    {
        try {
            $service = $this->serviceService->getCurrentService();

            $this->serviceService->stopCurrentService($service);
        } catch (Exception $e) {
        }

        $this->seatingService->isPreviousSeating();

        $takenNumbers = [];
        foreach ($seatingPlanRequest->plan as $table) {
            try {
                if (in_array($table['number'], $takenNumbers, true)) {
                    throw new \LogicException("Table number {$table['number']} already taken");
                }

                $takenNumbers[] = $table['number'];

                $this->seatingValidator->validate($table);
                $this->seatingService->persistSeat($table);
            } catch (Exception $e) {
                return $this->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(["message" => "OK"]);
    }

    #[Route("/api/seating", methods: [Request::METHOD_PUT])]
    public function seatClient(SeatClientRequest $seatClientRequest): Response
    {
        try {
            $currentService = $this->serviceService->getCurrentService();

            $table = $this->seatingService->getTableByNumber($seatClientRequest->number, $currentService);

            $this->restaurantServiceService->denyUnlessServiceIsStarted($table);

            $this->seatingService->seatClientToTable(
                $table,
                $seatClientRequest->takenSeats
            );
        } catch (Exception $e) {
            return $this->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
        return $this->json(["message" => "OK"]);
    }

    #[Route("/api/seating/leave", methods: [Request::METHOD_POST])]
    public function leaveSeat(LeaveSeatRequest $leaveSeatRequest): Response
    {
        try {
            $currentService = $this->serviceService->getCurrentService();

            $table = $this->seatingService->getTableByNumber($leaveSeatRequest->number, $currentService);

            if ($table->getTakenPlaces() === 0) {
                return $this->json(['message' => "Table has not been assigned"], Response::HTTP_BAD_REQUEST);
            }

            if ($leaveSeatRequest->tip) {
                $this->tipService->addTipToTable($table, $leaveSeatRequest->tip);
            }

            $this->seatingService->refreshSeats($table);
        } catch (Exception $e) {
            return $this->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['message' => 'OK']);
    }
}