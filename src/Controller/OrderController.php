<?php

namespace App\Controller;

use App\Entity\Dto\Input\OrderDTO;
use App\Service\OrderService;
use App\Service\SeatingService;
use App\Service\ServiceService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{

    public function __construct(
        private OrderService   $dishService,
        private SeatingService $seatingService,
        private ServiceService $serviceService
    )
    {
    }

    #[Route('/order/new', name: 'order.new', methods: Request::METHOD_POST)]
    public function newOrder(OrderDTO $orderRequestDTO): Response
    {
        try {
            $this->seatingService->denyUnlessSeatingPlanIsActive();
            $this->seatingService->denyUnlessTableIsTaken($orderRequestDTO->seating_number);

            $service = $this->serviceService->getCurrentService();

            $returnedOrder = $this->dishService->createOrder($orderRequestDTO, $service);

            return $this->json(["idOrder" => $returnedOrder->getId()], Response::HTTP_CREATED);
        } catch (Exception $e) {
            return $this->json(["message" => $e->getMessage()], Response::HTTP_BAD_REQUEST,);
        }
    }
}
