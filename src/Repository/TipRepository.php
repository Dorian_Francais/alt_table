<?php

namespace App\Repository;

use App\Entity\Seating;
use App\Entity\Tip;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tip|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tip|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tip[]    findAll()
 * @method Tip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tip::class);
    }

    public function addTipToTable(Seating $table, float $amount): void
    {
        $tip = new Tip();

        $tip->setLinkedTable($table);
        $tip->setAmount($amount);

        $this->_em->persist($tip);
        $this->_em->flush();
    }
}
