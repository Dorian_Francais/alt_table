<?php

namespace App\Repository\Exception;

use Exception;

class DishNotFoundInDBException extends Exception
{
    protected $message = 'Dish not found in DB.';
}
