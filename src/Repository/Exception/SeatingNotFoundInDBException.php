<?php

namespace App\Repository\Exception;

use Exception;

class SeatingNotFoundInDBException extends Exception
{
    protected $message = 'Seating not found in DB.';
}
