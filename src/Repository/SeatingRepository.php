<?php

namespace App\Repository;

use App\Entity\Seating;
use App\Entity\Service;
use App\Repository\Exception\SeatingNotFoundInDBException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Seating|null find($id, $lockMode = null, $lockVersion = null)
 * @method Seating|null findOneBy(array $criteria, array $orderBy = null)
 * @method Seating[]    findAll()
 * @method Seating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Seating::class);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function addSeating(array $table): void
    {
        $seating = new Seating();

        $seating->setNbOfPlaces($table['seatingPlaces']);
        $seating->setNumber($table['number']);

        $this->_em->persist($seating);
        $this->_em->flush();

    }

    public function checkForPreviousSeating(): bool
    {
        return count($this->findAll()) > 0;
    }

    public function cleanPreviousSeating(): void
    {
        $this
            ->createQueryBuilder('s')
            ->delete()
            ->getQuery()
            ->getResult();
    }

    public function getTableByNumber(int $numberOfTable, Service $service): Seating
    {
        $seating = $this
            ->createQueryBuilder('s')
            ->andWhere('s.number = :tableNumber')
            ->andWhere('s.linkedService = :service')
            ->setParameter('tableNumber', $numberOfTable)
            ->setParameter('service', $service)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$seating) {
            throw new SeatingNotFoundInDBException();
        }

        return $seating;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function seatClientToTable(Seating $table, int $numberOfClientToSeat): void
    {
        $table->setTakenPlaces($numberOfClientToSeat);
        $table->setIsTaken(true);
        $this->_em->flush();
    }

    public function getUnactiveSeats(): array
    {
        return $this->createQueryBuilder('s')
            ->where('s.linkedService IS NULL')
            ->getQuery()
            ->getResult();
    }

    public function linkToService(array $seats, Service $service): void
    {
        foreach ($seats as $seat) {
            $seat->setLinkedService($service);
        }

        $this->_em->flush();
    }

    public function refreshSeats(Seating $table): void
    {
        $table->setTakenPlaces(0);

        $this->_em->flush();
    }
}
