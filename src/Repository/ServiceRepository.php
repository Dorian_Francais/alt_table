<?php

namespace App\Repository;

use App\Entity\Service;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Service|null find($id, $lockMode = null, $lockVersion = null)
 * @method Service|null findOneBy(array $criteria, array $orderBy = null)
 * @method Service[]    findAll()
 * @method Service[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Service::class);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function launchService(): Service
    {
        $lastStartedService = $this->getLastStartedService();
        $lastStartedService?->setIsStarted(false);

        $service = new Service();

        $service->setIsStarted(true);

        $this->_em->persist($service);
        $this->_em->flush();

        return $service;
    }

    public function getLastStartedService(): ?Service
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.isStarted = :started')
            ->setParameter('started', true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function stopService(Service $service): Service
    {
        $service->setIsStarted(false);

        return $service;
    }
}
