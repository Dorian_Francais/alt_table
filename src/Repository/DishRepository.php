<?php

namespace App\Repository;

use App\Entity\Dish;
use App\Entity\Dto\Input\DishDTO;
use App\Repository\Exception\DishNotFoundInDBException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dish|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dish|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dish[]    findAll()
 * @method Dish[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DishRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dish::class);
    }

    public function findOneByName(string $name): ?Dish
    {
        return $this->findOneBy(['name' => $name]);
    }

    public function persistDish(DishDTO $dishRequestDTO): Dish
    {
        $dish = new Dish();
        $dish->setType($dishRequestDTO->type);
        $dish->setName($dishRequestDTO->name);
        $dish->setPrice($dishRequestDTO->price);
        $dish->setDescription($dishRequestDTO->description);
        $dish->setQuantity($dishRequestDTO->quantity);

        $this->_em->persist($dish);
        $this->_em->flush();
        return $dish;
    }

    public function persistEditDishQuantity(?int $quantity, ?string $name): ?Dish
    {
        if (!$name) {
            throw new DishNotFoundInDBException();
        }

        $dish = $this->findOneByName($name);

        if (!$dish) {
            throw new DishNotFoundInDBException();
        }

        $dish->setQuantity($quantity);
        $this->_em->flush();

        return $dish;
    }

    public function findAllDishes(): array
    {
        return $this->_em->getRepository(Dish::class)->findBy([], ['type' => 'ASC']);
    }

    public function findMenu(): array
    {
        $qb = $this->createQueryBuilder('p');

        $qb
            ->where(
                $qb->expr()->gt('p.quantity', 0)
            )
            ->orderBy('p.quantity', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function decrementDishQuantity(Dish $dish): Dish
    {
        $dish->setQuantity($dish->getQuantity() - 1);

        $this->_em->flush();

        return $dish;
    }
}
