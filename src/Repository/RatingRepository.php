<?php

namespace App\Repository;

use App\Entity\Dish;
use App\Entity\Dto\Input\RatingDTO;
use App\Entity\Rating;
use App\Entity\Seating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rating|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rating|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rating[]    findAll()
 * @method Rating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rating::class);
    }

    public function getExistingRatingByDishAndSeating(Dish $dish, Seating $seating): ?Rating
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.dish = :dish')->setParameter('dish', $dish)
            ->andWhere('r.seating = :seating')->setParameter('seating', $seating)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function persistAddRating(RatingDTO $ratingRequestDTO, Dish $dish, Seating $seating): Rating
    {
        $rating = new Rating();

        $rating->setRate($ratingRequestDTO->rate);
        $rating->setComment($ratingRequestDTO->comment);
        $rating->setDish($dish);
        $rating->setSeating($seating);

        $this->_em->persist($rating);
        $this->_em->flush();

        return $rating;
    }

    public function removeRating(Rating $existingRating): void
    {
        $this->_em->remove($existingRating);
        $this->_em->flush();
    }
}
