<?php

namespace App\Entity\Validator;

use App\Exception\Seating\NumberOfClientsHigherThanAvailableSeatsException;
use App\Exception\Seating\SeatingNotValidException;
use App\Exception\UnknownParameterException;

class SeatingValidator
{

    public function validate(array $seatingDto): void
    {
        foreach ($seatingDto as $key => $data) {
            switch ($key) {
                case 'number':
                    if (!is_integer(+$data)){
                        throw new SeatingNotValidException("La valeur de la clé number n'est pas valide");
                    }
                    break;
                case 'seatingPlaces':
                    if (!is_integer(+$data)){
                        throw new SeatingNotValidException("La valeur de la clé seatingPlaces n'est pas valide");
                    }
                    break;
                default:
                    throw new UnknownParameterException();
            }
        }
    }

}
