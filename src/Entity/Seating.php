<?php

namespace App\Entity;

use App\Repository\SeatingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SeatingRepository::class)
 */
class Seating
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private int $number = 1;

    /**
     * @ORM\Column(type="integer")
     */
    private int $nbOfPlaces = 0;

    /**
     * @ORM\Column(type="boolean", options={"default":0})
     */
    private bool $isTaken = false;

    /**
     * @ORM\Column(type="integer")
     */
    private int $takenPlaces = 0;

    /**
     * @ORM\ManyToOne(targetEntity=Service::class, inversedBy="seatings")
     */
    private ?Service $linkedService = null;

    /**
     * @ORM\OneToMany(targetEntity=Tip::class, mappedBy="linkedTable")
     */
    private $tips;

    /**
     * @ORM\OneToMany(targetEntity=Rating::class, mappedBy="seating")
     */
    private $ratings;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="seating")
     */
    private $orders;

    public function __construct()
    {
        $this->tips = new ArrayCollection();
        $this->ratings = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getNbOfPlaces(): int
    {
        return $this->nbOfPlaces;
    }

    public function setNbOfPlaces(int $nbOfPlaces): self
    {
        $this->nbOfPlaces = $nbOfPlaces;

        return $this;
    }

    public function getIsTaken(): bool
    {
        return $this->isTaken;
    }

    public function setIsTaken(bool $isTaken): self
    {
        $this->isTaken = $isTaken;

        return $this;
    }

    public function getTakenPlaces(): ?int
    {
        return $this->takenPlaces;
    }

    public function setTakenPlaces(int $takenPlaces): self
    {
        $this->takenPlaces = $takenPlaces;

        return $this;
    }

    public function getLinkedService(): ?Service
    {
        return $this->linkedService;
    }

    public function setLinkedService(?Service $linkedService): self
    {
        $this->linkedService = $linkedService;

        return $this;
    }

    /**
     * @return Collection|Tip[]
     */
    public function getTips(): Collection
    {
        return $this->tips;
    }

    public function addTip(Tip $tip): self
    {
        if (!$this->tips->contains($tip)) {
            $this->tips[] = $tip;
            $tip->setLinkedTable($this);
        }

        return $this;
    }

    public function removeTip(Tip $tip): self
    {
        if ($this->tips->removeElement($tip)) {
            // set the owning side to null (unless already changed)
            if ($tip->getLinkedTable() === $this) {
                $tip->setLinkedTable(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rating[]
     */
    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function addRating(Rating $rating): self
    {
        if (!$this->ratings->contains($rating)) {
            $this->ratings[] = $rating;
            $rating->setSeating($this);
        }

        return $this;
    }

    public function removeRating(Rating $rating): self
    {
        if ($this->ratings->removeElement($rating)) {
            // set the owning side to null (unless already changed)
            if ($rating->getSeating() === $this) {
                $rating->setSeating(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setSeating($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getSeating() === $this) {
                $order->setSeating(null);
            }
        }

        return $this;
    }
}
