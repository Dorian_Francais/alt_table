<?php

namespace App\Entity;

use App\Repository\TipRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TipRepository::class)
 */
class Tip
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="float")
     */
    private float $amount = 0;

    /**
     * @ORM\ManyToOne(targetEntity=Seating::class, inversedBy="tips")
     */
    private ?Seating $linkedTable = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $date;

    public function __construct()
    {
        $this->date = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getLinkedTable(): ?Seating
    {
        return $this->linkedTable;
    }

    public function setLinkedTable(?Seating $linkedTable): self
    {
        $this->linkedTable = $linkedTable;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
