<?php

namespace App\Entity;

use App\Repository\RatingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RatingRepository::class)
 */
class Rating
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="integer")
     */
    private int $rate = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $comment = null;

    /**
     * @ORM\ManyToOne(targetEntity=Seating::class, inversedBy="ratings")
     * @ORM\JoinColumn(nullable=false)
     */
    private Seating $seating;

    /**
     * @ORM\ManyToOne(targetEntity=Dish::class, inversedBy="ratings")
     * @ORM\JoinColumn(nullable=false)
     */
    private Dish $dish;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDishName(): ?int
    {
        return $this->dishName;
    }

    public function setDishName(int $dishName): self
    {
        $this->dishName = $dishName;

        return $this;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function setRate(int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getSeatingNumber(): ?int
    {
        return $this->seatingNumber;
    }

    public function setSeatingNumber(?int $seatingNumber): self
    {
        $this->seatingNumber = $seatingNumber;
    }
    public function getSeating(): ?Seating
    {
        return $this->seating;
    }

    public function setSeating(?Seating $seating): self
    {
        $this->seating = $seating;

        return $this;
    }

    public function getDish(): ?Dish
    {
        return $this->dish;
    }

    public function setDish(?Dish $dish): self
    {
        $this->dish = $dish;
        return $this;
    }
}
