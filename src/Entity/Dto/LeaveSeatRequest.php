<?php

namespace App\Entity\Dto;

use Nelexa\RequestDtoBundle\Dto\RequestBodyObjectInterface;

class LeaveSeatRequest implements RequestBodyObjectInterface
{
    public int $number;
    public ?float $tip;
}