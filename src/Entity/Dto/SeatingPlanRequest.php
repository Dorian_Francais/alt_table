<?php

namespace App\Entity\Dto;

use Nelexa\RequestDtoBundle\Dto\RequestBodyObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

class SeatingPlanRequest implements RequestBodyObjectInterface
{
    /**
     * @Assert\Type("array")
     */
    public array $plan = [];
}