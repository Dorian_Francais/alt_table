<?php

namespace App\Entity\Dto;

use Nelexa\RequestDtoBundle\Dto\RequestBodyObjectInterface;

class SeatClientRequest implements RequestBodyObjectInterface
{
    public int $number;
    public int $takenSeats;
}