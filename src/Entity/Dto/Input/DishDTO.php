<?php

namespace App\Entity\Dto\Input;


use Nelexa\RequestDtoBundle\Dto\RequestBodyObjectInterface;
use Nelexa\RequestDtoBundle\Dto\RequestObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

// cf https://github.com/Ne-Lexa/RequestDtoBundle
class DishDTO implements RequestBodyObjectInterface
{
    /**
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public ? string $name;

    /**
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $description;

    /**
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public ? string $type;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public float $price;

    /**
     * @Assert\Type(
     *     type="int",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public int $quantity = 0;
}
