<?php

namespace App\Entity\Dto\Input;


use Nelexa\RequestDtoBundle\Dto\RequestBodyObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

// cf https://github.com/Ne-Lexa/RequestDtoBundle
class OrderDTO implements RequestBodyObjectInterface
{
    /**
     * @Assert\Type(
     *     type="array",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public array $dishs;
    
    /**
     * @Assert\Type(
     *     type="integer",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public int $seating_number;

    /**
     * @Assert\Type(
     *     type="array",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public ?array $comments;

}
