<?php

namespace App\Entity\Dto\Input;


use Nelexa\RequestDtoBundle\Dto\RequestBodyObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

// cf https://github.com/Ne-Lexa/RequestDtoBundle
class EditDishDTO implements RequestBodyObjectInterface
{
    /**
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public ?string $name;

    /**
     * @Assert\Type(
     *     type="int",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public int $quantity = 0;
}
