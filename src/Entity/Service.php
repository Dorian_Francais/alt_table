<?php

namespace App\Entity;

use App\Repository\ServiceRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ServiceRepository::class)
 */
class Service
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $startDate;

    /**
     * @ORM\OneToMany(targetEntity=Seating::class, mappedBy="linkedService")
     */
    private Collection $seatings;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isStarted;

    public function __construct()
    {
        $this->startDate = new DateTime();
        $this->seatings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSeatings(): Collection
    {
        return $this->seatings;
    }

    public function addSeating(Seating $seating): self
    {
        if (!$this->seatings->contains($seating)) {
            $this->seatings[] = $seating;
            $seating->setLinkedService($this);
        }

        return $this;
    }

    public function removeSeating(Seating $seating): self
    {
        if ($this->seatings->removeElement($seating)) {
            // set the owning side to null (unless already changed)
            if ($seating->getLinkedService() === $this) {
                $seating->setLinkedService(null);
            }
        }

        return $this;
    }

    public function isStarted(): bool
    {
        return $this->isStarted;
    }

    public function setIsStarted(bool $isStarted): self
    {
        $this->isStarted = $isStarted;

        return $this;
    }
}
