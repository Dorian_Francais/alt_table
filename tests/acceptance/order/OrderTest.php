<?php

namespace App\Tests\acceptance\dish;

use App\DataFixtures\DishFixtures;
use App\DataFixtures\SeatingFixtures;
use App\Tests\WebTestCaseWithDatabase;
use DateTime;
use Symfony\Component\HttpFoundation\Request;

class OrderTest extends WebTestCaseWithDatabase
{

    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(SeatingFixtures::class);
        $this->addFixture(DishFixtures::class);
    }

    public const SEATING_PLAN = [
        'plan' => [
            [
                "number" => 1,
                "seatingPlaces" => 5
            ],
            [
                "number" => 2,
                "seatingPlaces" => 12
            ],
            [
                "number" => 3,
                "seatingPlaces" => 2
            ],
            [
                "number" => 4,
                "seatingPlaces" => 6
            ]
        ]
    ];

    public function testNewOrder(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, '/api/seating', self::SEATING_PLAN);

        $this->client->jsonRequest(Request::METHOD_POST, '/api/service', [
            "startDate" => (new DateTime())->format('d-m-Y')
        ]);

        $this->client->jsonRequest(Request::METHOD_PUT, '/api/seating', [
            "number" => 1,
            "takenSeats" => 3
        ]);

        $response = $this->client->jsonRequest(Request::METHOD_POST, '/order/new', [
            'dishs' => ['Boeuf1', 'Boeuf2', 'Boeuf1'],
            'comments' => ['saignant', 'a point', ''],
            'seating_number' => 1,
        ]);

        self::assertResponseIsSuccessful();
        // self::containsEqual(['name' => "Cote de boeuf"]);
    }
}
