<?php

namespace App\Tests\acceptance\rating;

use App\DataFixtures\DishFixtures;
use App\DataFixtures\SeatingFixtures;
use App\Tests\WebTestCaseWithDatabase;
use DateTime;
use Symfony\Component\HttpFoundation\Request;

class RatingTest extends WebTestCaseWithDatabase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(DishFixtures::class);
        $this->addFixture(SeatingFixtures::class);
    }

    public function testNewRating(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, '/api/dish', [
            'name' => 'Boeuf1',
            'description' => 'test description',
            'type' => 'Plat principal',
            'price' => 100.50,
            'quantity' => '20'
        ]);

        $this->client->jsonRequest(Request::METHOD_POST, '/api/service', [
            "startDate" => (new DateTime())->format('d-m-Y')
        ]);

        $this->client->jsonRequest(Request::METHOD_PUT, '/api/seating', [
            "number" => 1,
            "takenSeats" => 2
        ]);

        $this->client->jsonRequest(Request::METHOD_POST, '/order/new', [
            'dishs' => ['Boeuf1', 'Boeuf1'],
            'comments' => ['saignant', 'a point'],
            'seating_number' => 1,
        ]);

        $response = $this->client->jsonRequest(Request::METHOD_POST, '/api/dish/rating', [
            'dishName' => 'Boeuf1',
            'seatingNumber' => 1,
            'rate' => 5,
            'comment' => 'Mashallah c trop bon'
        ]);

        self::assertResponseIsSuccessful();
    }
}
