<?php

namespace App\Tests\acceptance\client;

use App\DataFixtures\SeatingFixtures;
use App\Exception\Seating\NumberOfClientsHigherThanAvailableSeatsException;
use App\Exception\Seating\TableAlreadyTakenException;
use App\Tests\WebTestCaseWithDatabase;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientTest extends WebTestCaseWithDatabase
{
    public const CONST_URL_SEATING = '/api/seating';

    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(SeatingFixtures::class);
    }

    public function testSeatClientToTable(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, '/api/service', [
            "startDate" => (new DateTime())->format('d-m-Y')
        ]);
        
        $this->client->jsonRequest(Request::METHOD_PUT, self::CONST_URL_SEATING, [
            'number' => 1,
            'takenSeats' => 2
        ]);

        self::assertResponseIsSuccessful();
    }

    public function testSeatTooManyClient(): void
    {
        $this->client->jsonRequest(Request::METHOD_PUT, self::CONST_URL_SEATING, [
            'number' => 2,
            'takenSeats' => 10
        ]);

        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST, NumberOfClientsHigherThanAvailableSeatsException::MESSAGE);
    }

    public function testSeatClientToAlreadyTakenTable(): void
    {
        $this->client->jsonRequest(Request::METHOD_PUT, self::CONST_URL_SEATING, [
            'number' => 1,
            'takenSeats' => 2
        ]);

        $this->client->jsonRequest(Request::METHOD_PUT, self::CONST_URL_SEATING, [
            'number' => 1,
            'takenSeats' => 2
        ]);

        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST, TableAlreadyTakenException::MESSAGE);
    }
}
