<?php

namespace App\Tests\acceptance\seating;

use App\Tests\WebTestCaseWithDatabase;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SeatingTest extends WebTestCaseWithDatabase
{
    public const CONST_URL_SEATING = '/api/seating';

    public const SEATING_PLAN = [
        'plan' => [
            [
                "number" => 1,
                "seatingPlaces" => 5
            ],
            [
                "number" => 2,
                "seatingPlaces" => 12
            ],
            [
                "number" => 3,
                "seatingPlaces" => 2
            ],
            [
                "number" => 4,
                "seatingPlaces" => 6
            ]
        ]
    ];

    public const UNVALID_SEATING_PLAN = [
        'plan' => [
            [
                "number" => 1,
                "seatingPlaces" => 5
            ],
            [
                "number" => 1,
                "seatingPlaces" => 12
            ]
        ]
    ];

    public function testCreateSeating(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, self::CONST_URL_SEATING, self::SEATING_PLAN);

        self::assertResponseIsSuccessful();
    }

    public function testCreateUnvalidSeating(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, self::CONST_URL_SEATING, self::UNVALID_SEATING_PLAN);

        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function testLeaveTableAndTip(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, self::CONST_URL_SEATING, self::SEATING_PLAN);

        $this->client->jsonRequest(Request::METHOD_POST, '/api/service', [
            "startDate" => (new DateTime())->format('d-m-Y')
        ]);

        $this->client->jsonRequest(Request::METHOD_PUT, self::CONST_URL_SEATING, [
            "number" => 1,
            "takenSeats" => 2
        ]);

        $this->client->jsonRequest(Request::METHOD_POST, '/api/seating/leave', ["number" => 1, "tip" => 5]);

        self::assertResponseIsSuccessful();
    }
}
