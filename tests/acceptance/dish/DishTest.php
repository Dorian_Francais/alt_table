<?php

namespace App\Tests\acceptance\dish;

use App\Tests\WebTestCaseWithDatabase;
use Symfony\Component\HttpFoundation\Request;

class DishTest extends WebTestCaseWithDatabase
{
    public const CONST_URL_DISH = '/api/dish';
    public const CONST_DESCRIPTION = 'test description';
    public const CONST_TYPE = 'Plat principal';
    
    public function testNewDish(): void
    {
       $this->client->jsonRequest(Request::METHOD_POST, self::CONST_URL_DISH, [
            'name' => 'Boeuf1',
            'description' => self::CONST_DESCRIPTION,
            'type' => self::CONST_TYPE,
            'price' => 100.50,
            'quantity' => '20'
        ]);
        
        self::assertResponseIsSuccessful();
    }

    public function testDeleteDish(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, self::CONST_URL_DISH, [
            'name' => 'Boeuf2',
            'description' => self::CONST_DESCRIPTION,
            'type' => self::CONST_TYPE,
            'price' => 100.50,
            'quantity' => '20'
        ]);

        $this->client->jsonRequest(Request::METHOD_DELETE, "/api/dish/remove/Boeuf2");

        self::assertResponseIsSuccessful();
        self::containsEqual(["message" => "OK"]);
    }

    public function testGetAllDishs(): void
    {
        $this->client->jsonRequest(Request::METHOD_GET, '/api/dishes');

        self::assertResponseIsSuccessful();

    }

    public function testGetAllDishsMenu(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, self::CONST_URL_DISH, [
            'name' => 'Boeuf3',
            'description' => self::CONST_DESCRIPTION,
            'type' => self::CONST_TYPE,
            'price' => 100.50,
            'quantity' => '20'
        ]);

        $this->client->jsonRequest(Request::METHOD_POST, self::CONST_URL_DISH, [
            'name' => 'Boeuf4',
            'description' => self::CONST_DESCRIPTION,
            'type' => self::CONST_TYPE,
            'price' => 100.50,
            'quantity' => '0'
        ]);

        $this->client->jsonRequest(Request::METHOD_POST, self::CONST_URL_DISH, [
            'name' => 'Boeuf5',
            'description' => self::CONST_DESCRIPTION,
            'type' => self::CONST_TYPE,
            'price' => 100.50,
            'quantity' => '1'
        ]);

        $this->client->jsonRequest(Request::METHOD_GET, '/api/menu');

        self::assertResponseIsSuccessful();
    }

    public function testUpdateDishQuantity(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, self::CONST_URL_DISH, [
            'name' => 'Boeuf1',
            'description' => self::CONST_DESCRIPTION,
            'type' => self::CONST_TYPE,
            'price' => 100.50,
            'quantity' => '20'
        ]);
        

        $this->client->jsonRequest(Request::METHOD_PUT, '/api/dish/edit',[
            'name' => 'Boeuf1',
            'quantity' => '17'
        ]);

        self::assertResponseIsSuccessful();
    }
}
