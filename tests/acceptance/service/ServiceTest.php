<?php

namespace App\Tests\acceptance\service;

use App\DataFixtures\SeatingFixtures;
use App\Tests\WebTestCaseWithDatabase;
use DateTime;
use Symfony\Component\HttpFoundation\Request;

class ServiceTest extends WebTestCaseWithDatabase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(SeatingFixtures::class);
    }

    public function testLaunchService(): void
    {
        $this->client->jsonRequest(Request::METHOD_POST, '/api/service');

        self::assertResponseIsSuccessful();
    }
}
