<?php

namespace App\Tests;

use ArgumentCountError;

class PingTest extends WebTestCaseWithDatabase
{
    public function testPing(): void
    {
        $this->client->request('GET', '/api/ping');

        self::assertResponseIsSuccessful();
        self::containsEqual(['message' => 'OK !']);

    }
}