# ALT_TABLE

Projet ALT'TABLE cours d'Archi

## Add your files
DAT => docs/DAT.docx
Test suite Postman => docs/ALTTABLE.postman_collection.json

## Test and Deploy
Postman + PHPUnit 
Pipeline d'intégration (gitlab CI/CD)
Analyse Sonar

## Authors
Julien Castéra / Marvin Aboulicam / Corentin Garnier / Gabin Depaire / Dorian Francais

